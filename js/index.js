
// let captcha;
// function generate() {
 
//     // Clear old input
//     document.getElementById("submit").value = "";
 
//     // Access the element to store
//     // the generated captcha
//     captcha = document.getElementById("image");
//     let uniquechar = "";
 
//     const randomchar =
// "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
//     // Generate captcha for length of
//     // 5 with random character
//     for (let i = 1; i < 5; i++) {
//         uniquechar += randomchar.charAt(
//             Math.random() * randomchar.length)
//     }
 
//     // Store generated input
//     captcha.innerHTML = uniquechar;
// }
 
// function printmsg() {
//     const usr_input = document
//         .getElementById("submit").value;
 
//     // Check whether the input is equal
//     // to generated captcha or not
//     if (usr_input == captcha.innerHTML) {
//         let s = document.getElementById("key")
//             .innerHTML = "Matched";
//         generate();
//     }
//     else {
//         let s = document.getElementById("key")
//             .innerHTML = "not Matched";
//         generate();
//     }
// }
function IsEmail(email) {
	var regex =
		/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!regex.test(email)) {
		return false;
	} else {
		return true;
	}
}
function IsPhone(phone_number) {
	
  const regex = /^(1\s?)?(\([0-9]{3}\)|[0-9]{3})[\s\-]?[0-9]{3}[\s\-]?[0-9]{4}$/;

	if (phone_number.length > 9 && regex.test(phone_number)) {
		return true;
	} else {
		return false;
	}
}
$(function () {
	
	$('#name').blur(function () {
		$('#name_error').hide();
	});
	$('#email').blur(function () {
		$('#email_error').hide();
		$('#invalid_email').hide();
	});
	$('#message').blur(function () {
		$('#message_error').hide();
	});
	$('#mobile').blur(function () {
		$('#mobile_error').hide();
	});

	$('#country').blur(function () {
		$('#country_error').hide();
	});
	
	
	$('#name_error').hide();
	$('#email_error').hide();
	$('#invalid_email').hide();
	$('#message_error').hide();
	$('#mobile_error').hide();
	
	$('#country_error').hide();
	$('#submitbtn').click(function () {
		var error = false;
		
		var name = $('#name').val();
		var email = $('#email').val();
		var message = $('#message').val();
    var mobile = $('#mobile').val();
    var country = $('#country').val();
		
		
		if (name == '') {
			$('#name_error').show();
			error = true;
		}
		if (email == '') {
			$('#email_error').show();
			error = true;
		} else if (email != '' && IsEmail(email) == false) {
			$('#email_error').hide();
			$('#invalid_email').show();
			error = true;
		} else {
			error = false;
		}
    if (IsPhone(mobile) == false) {
			$('#mobile_error').show();
			error = true;
		}
    if (country == '') {
			$('#country_error').show();
			error = true;
		}
		if (message == '') {
			$('#message_error').show();
			error = true;
		}
		if (error == true) {
			return false;
		}

		//ajax call php page
		$.post(
			'https://crm.kazma.co.in/Webservice/website_lead',
			$('#contactform').serialize(),
			function (response) {
        $('#txtMessage').html(response.message);
        $('#success').fadeIn('slow');
        $("#contactform")[0].reset();

        $("#contactform").get(0).reset();
        scrollTo(0,200)
				setInterval(()=>{
          $('#success').fadeOut('slow', function () {
            $('#txtMessage').html('');
            $('#contactform').fadeIn('slow');
          });
        },8000)
					
				
			}
		);
		return false;
	});
	
});

$('#closeButtonContact').click(function () {
  $('#success').fadeOut('slow', function () {
		$('#txtMessage').html('');
		$('#contactform').fadeIn('slow');
	});
});

